package polynomial

import (
	"fmt"
	"strconv"
	"strings"
	"unicode"
)

func SolvePolynomial(expr string, x int) int {
	split := strings.Split(expr, "=")
	if len(split) != 2 {
		panic("bad argument")
	}

	var (
		total, last int
		str         = []rune(split[1])
	)

	for i := 0; i < len(str); i++ {
		if str[i] != 'x' {
			continue
		}

		c := extractConstant(str[last:i])
		val := c * x

		last = i + 1

		// We are on x²
		if i+1 < len(str) && (str[i+1] != '+' && str[i+1] != '-') {
			val *= x
			i++
			last++
		}

		total += val
	}

	if monomial := str[last:]; len(monomial) > 0 {
		total += extractConstant(monomial)
	}

	return total
}

func extractConstant(monomial []rune) int {
	switch len(monomial) {
	case 0:
		return 1
	case 1:
		if !unicode.IsDigit(monomial[0]) {
			monomial = append(monomial, '1')
		}
	}

	c, err := strconv.Atoi(string(monomial))
	if err != nil {
		panic(fmt.Errorf("failed to extract constant: %v", err))
	}

	return c
}
