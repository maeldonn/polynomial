package polynomial

import (
	"testing"
)

func Benchmark_SolvePolynomial(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SolvePolynomial("f(x)=x²+x+1", 2)
	}
}

func Test_SolvePolynomial(t *testing.T) {
	type args struct {
		expr string
		x    int
	}
	tests := []struct {
		args args
		want int
	}{
		{
			args: args{expr: "f(x)=x²+x+1", x: 2},
			want: 7,
		},
		{
			args: args{expr: "g(x)=-x²-x-1", x: 2},
			want: -7,
		},
		{
			args: args{expr: "h(x)=2x²+2x+2", x: 2},
			want: 14,
		},
		{
			args: args{expr: "h(x)=-2x²-2x-2", x: 2},
			want: -14,
		},
		{
			args: args{expr: "f(x)=2x²", x: 2},
			want: 8,
		},
		{
			args: args{expr: "f(x)=x²+x", x: 2},
			want: 6,
		},
		{
			args: args{expr: "f(x)=x²+1", x: 2},
			want: 5,
		},
		{
			args: args{expr: "f(x)=x+1", x: 2},
			want: 3,
		},
	}

	for _, tt := range tests {
		t.Run(tt.args.expr, func(t *testing.T) {
			if got := SolvePolynomial(tt.args.expr, tt.args.x); got != tt.want {
				t.Errorf("SolvePolynomial() = %v, expected %v", got, tt.want)
			}
		})
	}
}
